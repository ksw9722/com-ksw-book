package com.kswtest.admin.web;

import com.kswtest.admin.domain.user.Role;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/error/404")
    public String error404(){
        return "ERROR404!!";
    }

    @GetMapping("/error")
    public String error(){
        return "Error!@#!!!";
    }

    @GetMapping("/error/500")
    public String error500(){
        return "ERROR500!!";
    }

    @GetMapping("/xss")
    public String xssTest(@RequestParam String id){
	    return "<h1> Hello "+id+"</h1>;
    }

    @GetMapping("/sqli")
    String sqliTest(@RequestParam String id){
        SessionFactory factory;
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        String sql = "SELECT name from kuser where id="+id;
        Session session = factory.openSession();
        SQLQuery query = session.createSQLQuery(sql);

        return "Hello kswtest!!";
    }

    @GetMapping("/api/authTest")
    public String authTest(HttpServletRequest request){

	    Cookie[] cookies = request.getCookies();
	    Cookie AuthCookie = null;
	    if(cookies!=null){
		    for(Cookie c:cookies){
			    if(c.getName()=="AuthCookie"){
				    AuthCookie = c;
				    break;
			    }

		    }

		    if(AuthCookie==null){
			    return "Not Authorized";
		    }else{
			    if(AuthCookie.getValue()=="1")
			    	return "<h1>Hello Admin!!</h1>;
			    else
				return "<h1> Not Authorized..!!</h1>";
		    }

	    }
	    return "<h1> Not Authorized.!!!</h1>";

	
    }

}
